# Tesseracket
Racket bindings for Google's tesseract-ocr, available from the [package listings](https://pkgs.racket-lang.org/#[tesseract]).

Example usage:
``` lisp
> (require main/tesseracket)
> (image->string "lorem.png")
"Lorem ipsum..."
> (image->string "lorem.png" #:out-base "foo" #:config "-nobatch numbers")
> (system "cat foo.txt")
1037 10 129
```

## Requirements
*Tesseracket was written in Racket 6.1.1*

[Install tesseract](https://github.com/tesseract-ocr/tesseract). You must be able to invoke tesseract at the shell, e.g. `user@host:~$ tesseract`
